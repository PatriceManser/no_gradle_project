# No_Gradle_Project

### This Project was createt without the help of Gradle or other Build Tools. It consists just out of Kotlin Code. 
### This Project was built to demonstrate the purpose and the benefits of Gradle. It should help to understand, that it is much more complicatet and slower if you do it by yourself, then letting Gradle do it for you.
#### The only thing this Project can, is to build and test itself and nothing else. 

## Build project
#### if you wanna build the project, you either have to run the build.sh file, or copy the text down below and paste it into the terminal window. To function correctly, you have to be in the No_Gradle_Project folder. You can change your location with cd.
 `kotlinc  src/test/kotlin/org/example/MyLibraryTest.kt -include-runtime -d output/MyLibraryTest.jar -cp Libraries/junit-4.13.jar:output/MyLibrary.jar`
 
 ## Test Project
 #### If you wanna test your build, you can either run the test.sh file, or copy the text down below and paste it into the terminal window. To function correctly, you have to be in the No_Gradle_Project folder. You can change your location with cd.
 `java   -cp "output/MyLibraryTest.jar:output/MyLibrary.jar:Libraries/junit-4.13.jar:Libraries/hamcrest-all-1.3.jar" org.junit.runner.JUnitCore org.example.MyLibraryTest`
 #### If you Test the Project, it will build by itself and is ready to test. 
 
 ###### Used Libraries: hamcrest-all-1.3.jar | JUnit-4.13.jar
 